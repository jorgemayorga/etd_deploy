var style = require('console-style');


function api_deploy_handler(req, res) {
    
    var repo =  req.params.repo;
    var branch =  req.params.branch;

    var commands = [
        ' cd ../ ',
        ' cd ' + repo ,
        ' git pull',
        ' ng build --prod --aot '
    ];

    console.clear();

    console.log(style.bold.red('--------------------------------'));
    console.log(' ');
    console.log(style.bold.red('   1.) repo : ') + repo);
    console.log(' ');
    console.log(style.bold.red('   2.) branch : ') + branch);
    console.log(' ');
    console.log(style.bold.red('---------------------------------'));
    console.log(' ');
    console.log(' ....  Running Commands : ');
    console.log(commands);
    console.log(' ');

    res.send('Hello World!');

}


/*
    dir = exec("git pull", function(err, stdout, stderr) { });
    dir = exec("ng build --prod --aot ", function(err, stdout, stderr) { });  
    dir = exec("", function(err, stdout, stderr) { });  
    dir.on('exit', function (code) {

    });
*/



module.exports = api_deploy_handler;
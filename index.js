/**
 * 
 * 
 */




 // ------------------------------------------------ //
 // --- Node.js App's Dependencies & Libraries ----- //
 // ------------------------------------------------ //

    var nodeExpress = require('express');
    var nodeStyle = require('console-style');
    var nodeExec = require('child_process').exec;

    var app = nodeExpress();

 // ------------------------------------------------ //





 // ------------------------------------------------ //
 // --- Helper's and API's Functions --------------- //
 // ------------------------------------------------ //

    const api_deploy_handler = require('./apis/deploy.js');

 // ------------------------------------------------ //

 

app.get('/api/ui/deploy/:repo/:branch', (req,res) => api_deploy_handler(req,res) );
app.get('/', (req,res) => {
   console.log('REQUEST GET /')
})

app.listen(80, function () {
  console.log('Example app listening on port 80');
});